json.array!(@recipes_ingredients) do |recipes_ingredient|
  json.extract! recipes_ingredient, :id, :recipe_id, :ingredient_id, :quantity, :unit
  json.url recipes_ingredient_url(recipes_ingredient, format: :json)
end
