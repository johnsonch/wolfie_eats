class RecipesIngredientsController < ApplicationController
  before_action :set_recipes_ingredient, only: [:show, :edit, :update, :destroy]

  # GET /recipes_ingredients
  # GET /recipes_ingredients.json
  def index
    @recipes_ingredients = RecipesIngredient.all
  end

  # GET /recipes_ingredients/1
  # GET /recipes_ingredients/1.json
  def show
  end

  # GET /recipes_ingredients/new
  def new
    @recipes_ingredient = RecipesIngredient.new
  end

  # GET /recipes_ingredients/1/edit
  def edit
  end

  # POST /recipes_ingredients
  # POST /recipes_ingredients.json
  def create
    @recipes_ingredient = RecipesIngredient.new(recipes_ingredient_params)

    respond_to do |format|
      if @recipes_ingredient.save
        format.html { redirect_to @recipes_ingredient, notice: 'Recipes ingredient was successfully created.' }
        format.json { render :show, status: :created, location: @recipes_ingredient }
      else
        format.html { render :new }
        format.json { render json: @recipes_ingredient.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /recipes_ingredients/1
  # PATCH/PUT /recipes_ingredients/1.json
  def update
    respond_to do |format|
      if @recipes_ingredient.update(recipes_ingredient_params)
        format.html { redirect_to @recipes_ingredient, notice: 'Recipes ingredient was successfully updated.' }
        format.json { render :show, status: :ok, location: @recipes_ingredient }
      else
        format.html { render :edit }
        format.json { render json: @recipes_ingredient.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recipes_ingredients/1
  # DELETE /recipes_ingredients/1.json
  def destroy
    @recipes_ingredient.destroy
    respond_to do |format|
      format.html { redirect_to recipes_ingredients_url, notice: 'Recipes ingredient was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_recipes_ingredient
      @recipes_ingredient = RecipesIngredient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def recipes_ingredient_params
      params.require(:recipes_ingredient).permit(:recipe_id, :ingredient_id, :quantity, :unit)
    end
end
