class Ingredient < ApplicationRecord
  has_many :recipes_ingredients
end
