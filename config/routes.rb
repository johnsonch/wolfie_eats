Rails.application.routes.draw do
  
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  resources :ingredients
  resources :recipes_ingredients
  
  resources :users do
    resources :recipes
  end

  get 'about' => 'static_pages#about'
  get 'opensource' => 'static_pages#opensource'
  root 'static_pages#home'
end
