require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get about" do
    get about_url
    assert_response :success
  end

  test "should get opensource" do
    get opensource_url
    assert_response :success
  end

  test "should have a root url" do
    get root_path
    assert_response :success
  end
end
