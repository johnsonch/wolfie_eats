require 'test_helper'

class RecipesIngredientsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @recipes_ingredient = recipes_ingredients(:one)
    @recipes_ingredient.recipe = recipes(:one)
    @recipes_ingredient.ingredient = ingredients(:one)
  end

  test "should get index" do
    get recipes_ingredients_url
    assert_response :success
  end

  test "should get new" do
    get new_recipes_ingredient_url
    assert_response :success
  end

  test "should create recipes_ingredient" do
    assert_difference('RecipesIngredient.count') do
      post recipes_ingredients_url, params: { recipes_ingredient: { ingredient_id: @recipes_ingredient.ingredient_id, quantity: @recipes_ingredient.quantity, recipe_id: @recipes_ingredient.recipe_id, unit: @recipes_ingredient.unit } }
    end

    assert_redirected_to recipes_ingredient_url(RecipesIngredient.last)
  end

  test "should show recipes_ingredient" do
    get recipes_ingredient_url(@recipes_ingredient)
    assert_response :success
  end

  test "should get edit" do
    get edit_recipes_ingredient_url(@recipes_ingredient)
    assert_response :success
  end

  test "should update recipes_ingredient" do
    patch recipes_ingredient_url(@recipes_ingredient), params: { recipes_ingredient: { ingredient_id: @recipes_ingredient.ingredient_id, quantity: @recipes_ingredient.quantity, recipe_id: @recipes_ingredient.recipe_id, unit: @recipes_ingredient.unit } }
    assert_redirected_to recipes_ingredient_url(@recipes_ingredient)
  end

  test "should destroy recipes_ingredient" do
    assert_difference('RecipesIngredient.count', -1) do
      delete recipes_ingredient_url(@recipes_ingredient)
    end

    assert_redirected_to recipes_ingredients_url
  end
end
