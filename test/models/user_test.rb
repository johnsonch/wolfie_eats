require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @valid_user = User.new(first_name: "Bart",
                     last_name: "Simpson",
                     password: 'foobar',
                     password_confirmation: 'foobar',
                     email: "bart@simpsons.com")
  end

  test "should be valid" do
    assert @valid_user.valid?
  end
  
  test "first name needs to be present" do
    @valid_user.first_name = nil
    assert_equal @valid_user.valid?, false
  end

  test "last name needs to be present" do
    @valid_user.last_name = nil
    assert_equal @valid_user.valid?, false
  end

  test "email to be present" do
    @valid_user.email = nil
    assert_equal @valid_user.valid?, false
  end 
end
